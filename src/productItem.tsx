// import React, { Component, ChangeEvent } from "react";
import React, { FunctionComponent, useState } from "react";
import { Product } from "./data/entities";

interface Props {
    product: Product,
    callback: (product: Product, quantity: number) => void
}

// interface State {
//     quantity: number
// }

// export class ProductItem extends Component<Props, State> {
export const ProductItem: FunctionComponent<Props> = (props) => {
//   constructor(props: Props) {
//     super(props);
//     this.state = { quantity: 1 };
//   }

    const [quantity, setQuantity] = useState<number>(1);

//   render() {
    return (
      <div className="card m-1 p-1 bg-light">
        <h4 className="d-flex justify-content-between">
          {props.product.name}
          <span className="badge rounded-pill bg-primary">
            ${props.product.price.toFixed(2)}
          </span>
        </h4>
        <div className="d-flex justify-content-between card-text bg-white p-1">
            <div>
                {props.product.description}
            </div>
            <div>
                <select
                    className="form-control-inline justify-content-right m-1"
                    onChange={(ev) => setQuantity(Number(ev.target.value))}>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                </select>
                <button
                    className="btn btn-success btn-sm justify-content-right"
                    onClick={ () => props.callback(props.product, quantity) }>
                    Add To Cart
                </button>
          </div>
        </div>
      </div>
    );
  }

//   handleQuantityChange = (ev: ChangeEvent<HTMLSelectElement>): void =>
//     this.setState({ quantity: Number(ev.target.value) });

//   handleAddToCart = (): void =>
//     this.props.callback(this.props.product, this.state.quantity);
// }